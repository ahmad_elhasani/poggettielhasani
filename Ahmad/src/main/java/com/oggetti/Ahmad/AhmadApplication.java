package com.oggetti.Ahmad;

import java.text.ParseException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.oggetti.Ahmad.service.Download;

@SpringBootApplication
public class AhmadApplication {

	//funzione principale che viene chiamata all'avvio dell'applicazione
	public static void main(String[] args) throws ParseException { 
		
			//Chiamata della funzione che andrà ad effettuare il download del file e il parsing dell'Url specificato
		    Download.dataDownload("http://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?file=data/educ_uoe_perp01.tsv.gz&unzip=true");
		    						
		    //Utilizzo del framework Spring Boot, tramite il quale si fa partire il run del progetto
		    SpringApplication.run(AhmadApplication.class, args); 
			
		}
	}

