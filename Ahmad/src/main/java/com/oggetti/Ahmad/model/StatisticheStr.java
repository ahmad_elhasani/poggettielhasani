package com.oggetti.Ahmad.model;

import java.util.ArrayList;

import com.oggetti.Ahmad.model.*;


//Effettuare i counting
public class StatisticheStr extends Statistiche {
	private int count;
	
	public StatisticheStr(ArrayList<Eurostat> eurostat, String fieldName, String value) {
		
		setCount(eurostat, fieldName, value);
	}
	
	public float getCount() {
		return count;
	}

	public void setCount(ArrayList<Eurostat> eurostat, String fieldName, String value) {

		int temp=0;
		if(value.equals("*"))
		{
			this.count=eurostat.size();
			return;
		}
		
		
		for(int i=0; i<eurostat.size(); i++) {
			
			if( ((String)pickMethod(eurostat.get(i),fieldName)).equals(value))
			{
				temp+=1;
			}
		}
		this.count=temp;
		
	}
}