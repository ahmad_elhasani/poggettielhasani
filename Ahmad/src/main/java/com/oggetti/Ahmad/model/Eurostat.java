package com.oggetti.Ahmad.model;

public class Eurostat {
	
	
	//elenco di tutti gli attributi presenti nel dataset
	protected String unit;
	protected String isced11;
	protected String age;
	protected String sex;
	protected String geoTime;
	protected float anno2017;
	protected float anno2016;
	protected float anno2015;
	protected float anno2014;
	protected float anno2013;
	
	
	//getter e setter di tutti gli attributi con visibilità protected
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getIsced11() {
		return isced11;
	}
	public void setIsced11(String iscend11) {
		this.isced11 = iscend11;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getGeoTime() {
		return geoTime;
	}
	public void setGeoTime(String geoTime) {
		this.geoTime = geoTime;
	}
	public float getAnno2017() {
		return anno2017;
	}
	public void setAnno2017(float anno2017) {
		this.anno2017 = anno2017;
	}
	public float getAnno2016() {
		return anno2016;
	}
	public void setAnno2016(float anno2016) {
		this.anno2016 = anno2016;
	}
	public float getAnno2015() {
		return anno2015;
	}
	public void setAnno2015(float anno2015) {
		this.anno2015 = anno2015;
	}
	public float getAnno2014() {
		return anno2014;
	}
	public void setAnno2014(float anno2014) {
		this.anno2014 = anno2014;
	}
	public float getAnno2013() {
		return anno2013;
	}
	public void setAnno2013(float anno2013) {
		this.anno2013 = anno2013;
	}
	
	//Costruttore della classe con tutti gli attributi come parametri
	public Eurostat(
			String Unit, String Isced11, String Age, String GeoTime, String Sex, Float argAnno2013, 
			Float argAnno2014, Float argAnno2015, Float argAnno2016, Float argAnno2017
			) 
	{
		this.unit = Unit;
		this.isced11 = Isced11;
		this.age = Age;
		this.geoTime= GeoTime;
		this.sex = Sex; 
		this.anno2013 = argAnno2013;
		this.anno2014 = argAnno2014;
		this.anno2015 = argAnno2015;
		this.anno2016 = argAnno2016;
		this.anno2017 = argAnno2017;
	}
}
