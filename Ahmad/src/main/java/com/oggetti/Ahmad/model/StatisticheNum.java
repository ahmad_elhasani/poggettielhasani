package com.oggetti.Ahmad.model;

import java.util.ArrayList;

import com.oggetti.Ahmad.model.*;

public class StatisticheNum extends Statistiche {

	// attributi
	private float avg;
	private float min;
	private float max;
	private float devStd;
	private float sum;

	public StatisticheNum(ArrayList<Eurostat> eurostat, String fieldName) {
		if(eurostat.isEmpty()) {
			this.avg=0;
			this.min=0;
			this.max=0;
			this.devStd=0;
			this.sum=0;
		}
		else{
		setSum(eurostat, fieldName);
		setAvg(eurostat);
		setMin(eurostat, fieldName);
		setMax(eurostat, fieldName);
		setDevStd(eurostat, fieldName);
		}
	}

	public float getAvg() {
		return avg;
	}

	public void setAvg(ArrayList<Eurostat> eurostat) {

		this.avg = this.sum / eurostat.size();

	}

	public float getMin() {
		return min;
	}

	public void setMin(ArrayList<Eurostat> eurostat, String fieldName) {
		float temp, value;
		temp = (float) pickMethod(eurostat.get(0), fieldName);
		for (int i = 1; i < eurostat.size() - 1; i++) {
			value = (float) pickMethod(eurostat.get(i), fieldName);
			if (value < temp)
				temp = value;
		}
		this.min = temp;

	}

	public float getMax() {
		return max;
	}

	public void setMax(ArrayList<Eurostat> eurostat, String fieldName) {
		float temp = 0, value = 0;
		for (int i = 0; i < eurostat.size() - 1; i++) {
			value = (float) pickMethod(eurostat.get(i), fieldName);
			if (value > temp)
				temp = value;
		}

		this.max = temp;

	}

	public float getDevStd() {
		return devStd;
	}

	public void setDevStd(ArrayList<Eurostat> eurostat, String fieldName) {
		for (int i = 0; i < eurostat.size(); i++) {
			float temp = (float) pickMethod(eurostat.get(i), fieldName) - getAvg();
			temp = temp * temp;
			temp = temp / eurostat.size();
			temp = (float) Math.sqrt(temp);
			this.devStd = temp;
		}
	}

	public float getSum() {
		return sum;
	}

	public void setSum(ArrayList<Eurostat> eurostat, String fieldName) {
		float temp = 0;
		for (int i = 0; i < eurostat.size(); i++) {
			temp = temp + (float) pickMethod(eurostat.get(i), fieldName);
		}

		this.sum = temp;
	}
}