package com.oggetti.Ahmad.model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.oggetti.Ahmad.*;

public class Statistiche {


	//Costruttore
	public Statistiche() {										
		
	}

	public static Object pickMethod(Eurostat r, String fieldName) {
		Object toReturn = null;
		//Prepara il nome del metodo (get + fieldName in cui si rende la prima maiuscola)
		String methodName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
		//Si ottiene un'istanza del metodo
		try {
			Method toInvoke = Eurostat.class.getMethod(methodName);
			toReturn = toInvoke.invoke(r, null);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			System.out.println("Metodo non esistente");
		} catch (SecurityException e) {
			e.printStackTrace();
			System.out.println("Security exception");
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return toReturn;

	}
}