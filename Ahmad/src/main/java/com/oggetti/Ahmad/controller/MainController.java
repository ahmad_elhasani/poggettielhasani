package com.oggetti.Ahmad.controller;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.oggetti.Ahmad.model.Eurostat;
import com.oggetti.Ahmad.model.EurostatMetadata;
import com.oggetti.Ahmad.model.Statistiche;
import com.oggetti.Ahmad.model.StatisticheNum;
import com.oggetti.Ahmad.model.StatisticheStr;
import com.oggetti.Ahmad.service.Download;



@RestController
public class MainController {

	//Creo le due arrayList
	ArrayList<EurostatMetadata> metadataArr = Download.getArrayMetadata();					
	ArrayList<Eurostat> recordArr = Download.getRecords();									

	/**Richesta di tipo GET di tutti i metadata.
	 * @return metadataArr, ritorna l'ArrayList<EurostatMetadata> 
	 * Il risultato sarà visualizzato in formato JSON
	 */
	@GetMapping("/metadata")
	public ArrayList<EurostatMetadata> sendMetadata() {
		return metadataArr;
	}


	/**Richista di tipo GET di tutti i dati del dataset
	 * @return metadataArr, ritorna l'ArrayList<Eurostat> 
	 * * Il risultato sarà visualizzato in formato JSON
	 */
	@GetMapping("/data")
	public ArrayList<Eurostat> sendGetData() {

		return recordArr;
	}

	
	/**Richiesta di tipo GET per ottenere i risultati delle statistiche, in base a dei parametri.
	 * I parametri saranno utilizzati per decidere su quale campo effettuare le statistiche.
	 * @param fieldName
	 * @param value
	 * @return stat
	 * @throws ResponseStatusException
	 */
	@GetMapping("/stats")
	public Statistiche sendStats(@RequestParam(name="fieldName", defaultValue = "null") String fieldName, @RequestParam (name="value", defaultValue = "null" )String value) 
	throws ResponseStatusException
	{
		Statistiche stat;
		if(fieldName.equals("null")) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Devi passare un parametro fieldName per ottenerne le statistiche.");
		
		//controllo 
		else if (fieldName.equals("unit") || fieldName.equals("isced11") || fieldName.equals("age")|| fieldName.equals("sex") || fieldName.equals("geoTime") ) 
		{
			//statistiche sulle stringhe
			stat = new StatisticheStr(recordArr, fieldName, value);
		} else {
			//statistiche numeriche
			stat = new StatisticheNum(recordArr, fieldName);
		}

		return stat;
	}
}