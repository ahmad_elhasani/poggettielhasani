package com.oggetti.Ahmad.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
//import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import java.util.ArrayList;

import com.oggetti.Ahmad.model.EurostatMetadata;
import com.oggetti.Ahmad.model.Eurostat;
import com.oggetti.Ahmad.service.Parser;

public class Download 
{
	private final static String ADDRESS_FILE="dataset.tsv";
	static ArrayList<Eurostat> eurostatArrayList;
	static ArrayList<EurostatMetadata> eurostatMetadataArrayList;
	
	
	
	
	/**Ottiene il json del link e lo interpreta.
	 * Chiama download e fa partire il download del dataset.tsv
	 * @param urlLink
	 * @throws ParseException
	 */
	
	public static void dataDownload(String urlLink) throws ParseException
	{
		String fileName = "dataset.tsv";

		try {
			
			// ottenere lo stream di una richiesta http get
			// Ottiene un collegamento con il link e mette tutto il contenuto della risposta nella variabile json
			URLConnection openConnection = new URL(urlLink).openConnection();
			openConnection.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
			InputStream in = openConnection.getInputStream();
			
			 String data = "";
			 String line = "";
			 try {
			   InputStreamReader inR = new InputStreamReader( in );
			   BufferedReader buf = new BufferedReader( inR );
			  
			   while ( ( line = buf.readLine() ) != null ) {
				   data+= line;
				   System.out.println( line );
			   }
			 } finally {
			   in.close();
			 }
			//Crea un oggetto JSONObject contenente tutto il json
			JSONObject obj = (JSONObject) JSONValue.parseWithException(data); 
			JSONObject objI = (JSONObject) (obj.get("result"));
			JSONArray objA = (JSONArray) (objI.get("resources"));
			
			for(Object o: objA){
			    if ( o instanceof JSONObject ) {
			        JSONObject o1 = (JSONObject)o; 
			        String format = (String)o1.get("format");
			        String urlD = (String)o1.get("url");
			        System.out.println(format + " | " + urlD);
			        if(format.toLowerCase().contains("tsv")) {
			        	//download(urlD, fileName);
			        	download(urlD, fileName);							
			        }
			    }
			}
			System.out.println( "OK" );
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally 
		{
			//Il parser del tsv crea l'ArrayList di Record
			eurostatArrayList = Parser.tsvParsing(ADDRESS_FILE);
			eurostatMetadataArrayList = Parser.getArrayMetadata();
		}
	}
	
	//effettua il download del file nel PATH specificato
	public static void download(String url, String fileName) throws Exception
 	{
		HttpURLConnection openConnection = (HttpURLConnection) new URL(url).openConnection();
		openConnection.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
		InputStream in = openConnection.getInputStream();
		 String data = "";
		 String line = "";
		 try {
		   if(openConnection.getResponseCode() >= 300 && openConnection.getResponseCode() < 400) {
			   download(openConnection.getHeaderField("Location"),fileName);
			   in.close();
			   openConnection.disconnect();
			   return;
		   }
		   Files.copy(in, Paths.get(fileName));
		   System.out.println("File size " + Files.size(Paths.get(fileName)));  
		 } finally {
		   in.close();
		 }
	}
		
	public static ArrayList<Eurostat> getRecords()
	{
		return eurostatArrayList;
	}
		
	public static ArrayList<EurostatMetadata> getArrayMetadata() 
	{
		return eurostatMetadataArrayList;
	}
	
}


