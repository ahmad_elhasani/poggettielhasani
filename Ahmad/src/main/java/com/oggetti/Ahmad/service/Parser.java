package com.oggetti.Ahmad.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.lang.ArrayUtils;

import java.lang.Object;
import com.oggetti.Ahmad.model.*;
import com.oggetti.Ahmad.service.*;

public class Parser {

	public static  ArrayList<EurostatMetadata> getArrayMetadata() 
	{
		// TODO Auto-generated method stub
		
			ArrayList<EurostatMetadata> eurostatMetadataArrayList= new ArrayList<EurostatMetadata>();
			
			eurostatMetadataArrayList.add(new EurostatMetadata("unit", "Unita", "String"));
			eurostatMetadataArrayList.add(new EurostatMetadata("isced11", "Isced11", "String"));
			eurostatMetadataArrayList.add(new EurostatMetadata("age", "Eta", "String"));
			eurostatMetadataArrayList.add(new EurostatMetadata("sex", "Sesso", "String"));
			eurostatMetadataArrayList.add(new EurostatMetadata("geoTime", "GeoEta", "String"));
			eurostatMetadataArrayList.add(new EurostatMetadata("anno2017", "Anno", "Float"));
			eurostatMetadataArrayList.add(new EurostatMetadata("anno2016", "Anno", "Float"));
			eurostatMetadataArrayList.add(new EurostatMetadata("anno2015", "Anno", "Float"));
			eurostatMetadataArrayList.add(new EurostatMetadata("anno2014", "Anno", "Float"));
			eurostatMetadataArrayList.add(new EurostatMetadata("anno2013", "Anno", "Float"));
			
			return eurostatMetadataArrayList;
	}	
	
	
    public static String[] combine(String[] a, String[] b)
    {
    	int lengthOfBoth = a.length + b.length;
        String length = Integer.toString(lengthOfBoth);
        String[] result = new String[lengthOfBoth];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }


    //Parsing 
	public static ArrayList<Eurostat> tsvParsing(String tsvFile)
	{		
		ArrayList<Eurostat> eurostatLocalArrayList = new ArrayList<Eurostat>();

			try 
			{
	
				BufferedReader br = new BufferedReader(new FileReader(tsvFile));
				br.readLine(); // legge la prima riga e la ignora
				String line;
				
				while ((line = br.readLine()) != null) 
				{		
					
					try 
					{
	
						/**Il database ha errori nella struttura, si rimpiazzano quindi le virgole con dei punti e virgola e siccome
						 * ho bisogno di float invece che stringhe ho dovuto eliminare gli spazi in ogni campo.
						 */
						//valori cambiati per le statistiche (parsing)
						line=line.replaceAll("d", " ");
						line=line.replaceAll(":", "0");
						//line=line.replaceAll("", "0");		//i valori nulli vengono settati a zero per le statistiche
						
						
						//split diversi per virgole e tab
				        String[] recordCorrente = line.split(",");
				        String[] lastCorrente= recordCorrente[4].split("\t");
				        recordCorrente[4] = lastCorrente[0];
				        Arrays.copyOfRange(lastCorrente, 1, lastCorrente.length);
				        String[] both = (String[])ArrayUtils.addAll(recordCorrente, lastCorrente); 
				        
				        /** Crea un oggetto di tipo Record.
				         * Elimina eventuali spazi vuoti all'inizio delle Stringhe 
				         */
				        Eurostat oggettoRecord = new Eurostat(
				        							recordCorrente[0],		
				        							recordCorrente[1],		
				        							recordCorrente[2],		
				        							recordCorrente[3],
				        							recordCorrente[4],
				        									
				        							Float.valueOf(recordCorrente[5]),		
				        							Float.valueOf(recordCorrente[6]),		
				        							Float.valueOf(recordCorrente[7]),		
				        							Float.valueOf(recordCorrente[8]),		
				        							Float.valueOf(recordCorrente[9])		
						);
				        
				        eurostatLocalArrayList.add(oggettoRecord);
				        
					}   //end of the try
					
					catch(ArrayIndexOutOfBoundsException e) 
					{} 
					
					catch(NumberFormatException e) 
					{} 
				
				} //end of the while
		     
				try 
				{
					br.close();
		        } 
				catch (IOException e) 
				{
		        	System.out.println(e.getClass().getCanonicalName());
		    	}
		       
	
				return eurostatLocalArrayList;
	
			}	//end of the outer try
			
			catch (FileNotFoundException e) 
			{
				System.out.println(e.getClass().getCanonicalName());		
			} 
			catch (IOException e) 
			{
				System.out.println(e.getClass().getCanonicalName());
			}
			
			return eurostatLocalArrayList;
	}

}

