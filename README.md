# ProgettoOggettiELHasani

Un Web Service è un sistema software progettato per supportare l'interoperabilità tra diversi elaboratori su una medesima rete, è in grado di mettersi al servizio di un Client (applicazione, sito web, Postman) comunicando mediante il protocollo http. Consente quindi agli utenti che vi si collegano di usufruire delle funzioni che mette a disposizione. Con Spring Boot, è stato possibile creare questo software che lancia l'intera applicazione web, compreso il web server integrato.

L'applicazione permette di richiedere mediante Http request di tipo GET:

-   Restituzione dei metadati, formato JSON, ovvero l’elenco degli attributi, alias degli stessi e tipo di dati contenuti.
-   Restituzione dei dati riguardanti ogni record, formato JSON.
-   Restituzione delle statistiche sui dati di uno specifico campo.

Il progetto è diviso in packeges che a loro volta sono suddivisi in più classi. 
Per quanto riguarda le statistiche utilizzano le classi StatisticheStr e StatisticheNum che estendo la classe Statistiche.

### Richieste GET

Per eseguire le richieste GET si può installare un API testing (come Postman_). 


Esempio di prova: 

*Valori stringa* con **GET** 
 -  localhost:8080/stats?fieldName=sex&value=F 
 -  localhost:8080/data?fieldName=sex&value=M 
 

*Valori numerici* con **GET** 
 - localhost:8080/stats?fieldName=anno2017 
 - localhost:8080/data?fieldName=anno2013
 